#include "catch.hpp"        //include testing framework
#include "../src/getN.hpp"  //include the module to be tested

#include <iostream>
#include <fstream>





TEST_CASE("getN TEST @valakhar")
{
   std::ifstream in;
   
   //redirect cin to read from the file "cin-1.txt" 
   in.open("tests/cin-1.txt");
   auto cin_buff = std::cin.rdbuf();
   std::cin.rdbuf(in.rdbuf());
   //-----------------------------------
   
   
  if(in)
  {
    int n = getN( );
    CHECK(( n > 0 and n < 1001)); //perform test
  }
  else
     std::cout<<"** COULD NOT READ FROM FILE 'cin-1.txt' **\n"
  
  
  
  //redirect cin to read from another file for second/third/fourth test
  // ...
  
  
  
  
  // restore cin as the last step of this function
   std::cin.rdbuf(cin_buff);
}


