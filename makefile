# version 1.2.04.06.2019

CPPFLAGS=-std=c++14 -Wall 
CC=g++
C2FLGS=--success --use-colour yes

	

run:
	@bin/tests.out $(C2FLGS) || true #force make to be silent


tests: bin/tests.o
	@printf "\033[33mCompiling Tests...\n\033[0m"	
	$(CC) $(CPPFLAGS) -o bin/tests.out bin/tests.o tests/avgPrimeTest.cpp tests/isPrimeTest.cpp tests/numDigitsTest.cpp tests/getAverageTest.cpp tests/primeDigitsTest.cpp tests/getNTest.cpp

bin/tests.o: tests/*.cpp src/*.hpp 
	@printf "\033[36mCompiling Test Driver...(be patient)...\n\033[0m"	
	$(CC) $(CPPFLAGS)  -c tests/tests_main.cpp -o bin/tests.o

clean:
	@printf "\033[31mRemoving objects (and temporary files)\033[0m\n"
	@rm -rf bin/*.o*
