int numDigits()
{
	int n;
	int count = 0;
	
	cout << "Please enter an integer: "; <<endl;
	cin >> n;
	
	while(n != 0)
	{
		n /= 10;
		++count;
	}
	
	cout << "Number of digits: ", count; <<endl;
}
