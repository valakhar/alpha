int getAverage()
{
	vector<float> u;
	float input;
	
	cout << "Please enter the numbers you want to find the average of: " <<endl;
	while ( cin >> input)
		u.push_back(input);
		
	double average = accumulate( u.begin(), u.end(), 0.0)/u.size();
	cout << "The average is : " << average << endl;
}
